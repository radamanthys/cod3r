import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'provider/cart.dart';
import 'provider/orders.dart';
import 'provider/products.dart';
import 'utils/app_routes.dart';
import 'views/cart_page.dart';
import 'views/orders_page.dart';
import 'views/product_detail_page.dart';
import 'views/product_form_page.dart';
import 'views/products_overview_page.dart';
import 'views/products_page.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => Products()),
        ChangeNotifierProvider(create: (_) => Cart()),
        ChangeNotifierProvider(create: (_) => Orders()),
      ],
      child: MaterialApp(
        title: 'Minha Loja',
        theme: ThemeData(
          primarySwatch: Colors.purple,
          accentColor: Colors.deepOrange,
          fontFamily: 'Lato',
        ),
        debugShowCheckedModeBanner: false,
        // home: ProductOverviewPage(),
        routes: {
          AppRoute.HOME: (_) => ProductOverviewPage(),
          AppRoute.PRODUCT_DETAIL: (_) => ProductDetailPage(),
          AppRoute.CART: (_) => CartPage(),
          AppRoute.ORDERS: (_) => OrdersPage(),
          AppRoute.PRODUCTS: (_) => ProductsPage(),
          AppRoute.PRODUCT_FORM: (_) => ProductFormPage(),
        },
      ),
    );
  }
}
